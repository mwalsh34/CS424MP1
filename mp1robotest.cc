#include <SerialStream.h>
#include "irobot-create.hh"
#include <ctime>
#include <iostream>
#include <chrono>
#include <thread>
#include <raspicam/raspicam_cv.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdlib.h>

using namespace iRobot;
using namespace LibSerial;
using namespace std;

//typedef std::pair<unsigned char, unsigned char> note_t;
//typedef std::vector<note_t> song_t;

int main()
{
	char serial_loc[] = "/dev/ttyUSB0";
	try {
		raspicam::RaspiCam_Cv Camera;
		cv::Mat rgb_image, bgr_image;
		if (!Camera.open()){
			cerr << "Error opening the camera" << endl;
			return -1;
		}
	
		cout << "Opened Camera" << endl;
		SerialStream stream (serial_loc, LibSerial::SerialStreamBuf::BAUD_57600);
		cout << "Opened Serial Stream to" << endl;
		this_thread::sleep_for(chrono::milliseconds(1000));
		Create robot(stream);
		cout << "Created iRobot Object" << endl;
		robot.sendFullCommand();
		cout <<  "Setting iRobot to full mode" << endl;
		this_thread::sleep_for(chrono::milliseconds(1000));
		cout << "Robot is ready" << endl;

		Create::sensorPackets_t sensors;
		sensors.push_back(Create::SENSOR_BUMPS_WHEELS_DROPS);
		sensors.push_back(Create::SENSOR_WALL_SIGNAL);
		sensors.push_back(Create::SENSOR_BUTTONS);

		robot.sendStreamCommand(sensors);
		cout << "Sent Stream Command" << endl;

		int speed = 287;
		robot.sendDriveCommand(speed, Create::DRIVE_STRAIGHT);
		cout << "Sent Drive Command" << endl;

		short wallSignal, prevWallSignal = 0;

		unsigned char note1 = 0;
		unsigned char duration = 1;
		unsigned char sid  = 0;
		int radius = 0;
		int fractionOfRevolution = 0;
		int revTime = 0;
		int turnRadius = 0;
		//typedef std::pair<unsigned char, unsigned char> note_t note = pair(note1, duration);
	        //typedef std::vector<note_t> song_t song = pair(note);	
		//note_t * note = pair<unsigned char, unsigned char>(note1, duration);
		//song_t song = vector<note_t>(note1, duration);
		int ledOn = 0;

		while (!robot.playButton()){
			// Code here for wallSignal
			//if (wallSignal > 0){
				//cout << "Wall Signal" << endl;
				//robot.sendSongCommand(sid, (0);
				//robot.sendPlaySongCommand(sid);
				//robot.sendDriveCommand(0, Create::DRIVE_STRAIGHT);
			//}
			if (robot.bumpLeft() || robot.bumpRight()){
				cout << "Bump!" << endl;
				speed = 0;
				robot.sendDriveCommand(speed, Create::DRIVE_STRAIGHT);
				// Start led stuff
				ledOn = 1;
				

				//Reverse 15 iches at 165 mm/s
				speed = 165;
				robot.sendDriveCommand(-speed, Create::DRIVE_STRAIGHT);
				sleep(2.391);	
				robot.sendDriveCommand(0, Create::DRIVE_STRAIGHT);
				//Take picture
				Camera.grab();
				Camera.retrieve (bgr_image);
				cv::cvtColor(bgr_image, rgb_image, CV_RGB2BGR);
				cv::imwrite("irobot_image.jpg", rgb_image);
				cout << "Taking photo" << endl;

				//STop leds
				

				//Rotate random angle between 120 and 240 degrees at 107 mm/s
				radius = rand()%(120 + 1) + 120;
				speed = 107;
				fractionOfRevolution = radius/360;
				turnRadius = fractionOfRevolution * (159.512);
				revTime = (turnRadius/speed);
				robot.sendDriveCommand(speed, Create::DRIVE_INPLACE_CLOCKWISE);
				sleep(revTime);

				//Start moving again at original speed
				speed = 287;
				robot.sendDriveCommand(speed, Create::DRIVE_STRAIGHT);


				// Keep cycle going

			}	
		this_thread::sleep_for(chrono::milliseconds(100));
		}
	}
	catch (InvalidArgument& e){
		cerr << e.what () << endl;
		return 3;
	}
	catch (CommandNotAvailable& e){
		cerr << e.what () << endl;
		return 4;
	}

}
